## Interface: 11506
## Title: ActionMirroringFrame
## Notes: Shows up pressed keys and relative actions
## Version: 0.1
## Author: WobLight
## SavedVariables: ActionMirroringFrameSettings
## SavedVariablesPerCharacter: ActionMirroringFrameProfile
## Dependencies: EmeraldFramework
ActionMirroringFrame.lua
